"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var user_component_1 = require("./components/user.component");
var about_component_1 = require("./components/about.component");
var category_component_1 = require("./components/category.component");
var search_component_1 = require("./components/search.component");
var appRoutes = [
    {
        path: '',
        component: user_component_1.UserComponent
    },
    {
        path: 'about/:id/:country/:category/:slug',
        component: about_component_1.AboutComponent,
        data: {
            meta: {
                title: 'About page',
                description: 'Description of the home page'
            }
        }
    },
    {
        path: 'category/:searchTerm/:id',
        component: category_component_1.CategoryComponent
    },
    {
        path: 'search/:searchTerm',
        component: search_component_1.SearchComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map