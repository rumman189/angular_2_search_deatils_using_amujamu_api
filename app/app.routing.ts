import {ModuleWithProviders} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';

import {UserComponent} from './components/user.component';
import {AboutComponent} from './components/about.component';
import {CategoryComponent} from './components/category.component'
import {SearchComponent} from './components/search.component';

const appRoutes: Routes = [
    {
        path:'',
        component: UserComponent



    },
    {
        path:'about/:id/:country/:category/:slug',
        component: AboutComponent,
        data: {
            meta: {
                title: 'About page',
                description: 'Description of the home page'
            }
        }
    },
    {
        path:'category/:searchTerm/:id',
        component: CategoryComponent

    },
    {
        path:'search/:searchTerm',
        component: SearchComponent

    }



];
export const routing: ModuleWithProviders =RouterModule.forRoot(appRoutes);
