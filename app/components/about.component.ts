import { Component,OnInit,AfterViewInit } from '@angular/core';
import {TourService} from '../services/abouttour.service';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';





@Component({
    moduleId:module.id,
    selector: 'about',
    template: 'about.component.html',
//     selector: 'about',
//     template:`
//
//
// <head>
//     <title>angular5 | Take a trip, Take a breath</title>
//     <base href="/">
//     <META NAME="Title" CONTENT="angular5 | Take a trip, Take a breath">
//     <META NAME="Description" CONTENT="angular2 is an innovative services at any time from any location across the globe. Stay tension free about your tour bookings.">
//     <link rel="shortcut icon" href="./home-bg.ico" alt="image"/>
//
// </head>
//     <h3>Tour Details</h3>
//     <p>{{title}}</p>
//     <div class="rateYo"></div>
//     <p>{{rating}}</p>
//     <h3>Description</h3>
//     <p>{{description}}</p>
//     <h3>Cancellation Policy</h3>
// <p>{{cancel}}</p>
//
//
// <div>
//         <h3>Country</h3>
//         <p>{{country}}</p>
//         <h3>Duration</h3>
//         <p>{{duration}}</p>
// </div>
//
//         <h3>Food </h3>
//         <div *ngFor="let food of foods">
//           <p>{{food.name}}</p>
//
//         </div>
//
//
//
//       <div>
//         <h3>Feature </h3>
//
//        <p>{{feature}}</p>
//
//         </div>
//         <hr/>
//  <div class =row>
//
//
// <h2>Review </h2>
//
//         <div *ngFor="let review of reviews">
//         <div class =row>
//          <div class="col-sm-3">
//        <a href="#">
//                         <img class="img-responsive" src="../././img/post-sample-image.jpg" alt="" height="100px" width="500px">
//                     </a>
//                     </div>
//         <div class="col-sm-6">
//         <p> {{review.created_at | date:'medium'}}</p>
//           <p>{{review.user.email}}</p>
//           <hr/>
//           <h4>Comment :</h4>
//           <h5>{{review.comment}}</h5>
//            <hr/>
//           </div>
//
//           <div class="col-sm-3">
//           <h3>Rating</h3>
//           <p>{{review.rating}}</p>
// </div>
//           </div>
//           </div>
//
//
//
//         </div>
//
//
//      `,


    providers: [TourService,Title]
})




export class AboutComponent  {


    id:number;
    //title: string;

    description :string;
    adress:string;
    country:string;
    duration:string;
    cancel:string;
    tours : Tour[];
    feature:any [];
    foods : Food[];
    category:any;
    slug:any;
    sub: any
    rating:any;
    subscription:any;
    reviews:Reviews[];
    Title:any;
event:any;




   public constructor(private tourService :TourService, private route: ActivatedRoute
    ){
        document.title = "Hello World";
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id'];
            this.country = params['country'];
            this.category = params['category'];
            this.slug = params['slug'];
        });

        this.tourService.getTour(this.id,this.country,this.category,this.slug).subscribe(
            tours=>
            {             console.log(tours);
                //this.title=tours.tour.title,
                    this.description=tours.tour.description,
                    this.country=tours.tour.country.country;
                this.duration=tours.tour.tour_duration.duration;
                this.foods=tours.tour.foods;
                this.feature=tours.tour.included_features;
                this.cancel=tours.tour.cancellation_policy;
                this.rating=tours.tour.rating;
                this.reviews=tours.tour.reviews;
            });
        // console.log(this.tours);

        // title.setTitle('My App - Page Title');
        document.title = "Hello World";
    }
    // ngAfterViewInit() {
    //
    //     this.tourService.getTour(this.id,this.country,this.category,this.slug).subscribe(
    //         tours=>this.rating=tours);
    //     console.log(this.rating);
    //     $(document).ready(function () {
    //
    //         $(".rateYo").rateYo({
    //             rating:this.rating,
    //             readOnly: true
    //         });
    //
    //     });
    //
    //
    // }

}


interface Tour{

    id :any;
    name :string;

}

interface Food{

    id :any;
    name :string;

}

interface Reviews{
    created_at:any;
    id :any;
    rating :any;

}




