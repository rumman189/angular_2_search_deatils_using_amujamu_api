import { Component } from '@angular/core';
import {TourService} from '../services/category.service';
import { Http, Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'user',
    template: `
<div class="row">
<div class="col-sm-3">
<!--<input class="form-control"name="search" #searchTerm (keyup.enter)="performSearch(searchTerm)">-->
        <!---->
<!--<button type="button" class="btn btn-success" (click)="performSearch(searchTerm)">Search</button><hr/>-->
<br/>
<h5>Filter By Categories </h5>
<ul>
<li *ngFor="let t of mytours" >
  <a [routerLink]="['/category',t.country.country,t.category.id]">{{t.category.name}} </a>
                    <!--<li>-->
            <!--<span ></span>-->
                            <!--<a href="">Spot Visit </a>-->
            <!--</li>-->
                    <!--<li>-->
            <!--<span></span>-->
                            <!--<a href="">Day Tour </a>-->
            <!--</li>-->
                    <!--<li>-->
            <!--<span></span>-->
                            <!--<a>Nature </a>-->
            <!--</li>-->
            </ul>
            
</div>
<!--<div *ngFor="let tour of tours">-->
<!--<li><a routerLink="/about" >{{tour.title}}</a>-->

<!--</li>-->
<!--</div>-->
<!--<a routerLink="/about" >Details</a>-->
<!--<div>-->



<div class="col-sm-8">
<div class="row">
 
<div class="jumbotron"  *ngFor="let t of mytours"
  [routerLink]="['/about', t.id,t.country.country,t.category.slug,t.slug]">
  <div class="row">
  <div class="col-sm-3">
  <a href="#">
                        <img class="img-responsive" src="../././img/post-sample-image.jpg" alt="" height="100px" width="500px">
                    </a>
                    </div>
                    <div class="col-sm-6">
 <a><b><h3>{{ t.title }}</h3></b></a>
 <h5>Reviews: {{t.total_rating}}</h5><br/>
 <h5>{{t.description}}</h5><br/>
 
 <i  class="fa fa-bus" aria-hidden="true"></i>
<i class="fa fa-child" aria-hidden="true"></i>
<i class="fa fa-home" aria-hidden="true"></i>
<i class="glyphicon glyphicon-envelope"></i>
 </div>
 <div class="col-sm-2">
    <h6>Amujamu Price</h6>
   <h5>{{t.default_price}}</h5>
   <h6>Market Price</h6>
 <h5><font color="red"><del>{{t.original_market_price}}</del></font></h5><br/>

  <a><button type="button" class="btn btn-primary btn-xs">Details</button></a>
  
  </div>
  </div>
 </div>
 </div>
 </div>
 </div>
 
 <!--<br/> {{t.default_price}}-->
 <!--<br/> {{t.original_market_price}}-->
 <!--<br/> {{t.total_rating}}-->

   <!--<i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i>-->
<!--<span class="sr-only">Refreshing...</span>-->
<!--<i class="fa fa-bus" aria-hidden="true"></i>-->
<!--<i class="fa fa-child" aria-hidden="true"></i>-->
<!--<i class="fa fa-home" aria-hidden="true"></i>-->
 

 
 
  
  
   





   <!--<div *ngFor="let p of mytours">-->
<!--<p>{{p.slug}}</p>-->
  <!---->

        <!---->
         <!---->
          <!---->
        <!--</div>-->
          <!---->
       <!---->
  <!--</div>-->
  <!---->
  
<!--<div class="clearfix"></div>-->
    <!--<div class="itemscontainer offset-1">-->
                    <!--<div class="offset-2">-->
                <!--<div class="col-md-4 offset-0">-->
                    <!--<div class="listitem2">-->
                                                    <!--<a href="">-->
                                <!--<img src="" alt="A tour to Bangkok Opera"/>-->
                            <!--</a>-->
                                            <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-8 offset-0">-->
                    <!--<div class="itemlabel3">-->
                        <!--<div class="labelright">-->
                            <!--<div class="rate-star no-padding" data-rating-value="2.8"></div>-->
                            <!--<span class="size11 grey">1 Reviews</span>-->
                            <!--<br><br>-->

                                <!--<span class="size11 grey">Amujamu Price</span><br>-->
                                <!--<span class="green size16"><b>$90.21</b></span><br>-->
                            <!---->
                            <!--<div class="clearfix"></div>-->
                                <!--<span class="size11 grey">Market Price</span><br>-->
                                <!--<span class="red size16">-->
                                    <!--<strike>-->
                                        <!--$86.33-->
                                    <!--</strike>-->
                                <!--</span>-->
                                <!--<br/>-->
                                                        <!--<br>-->
                            <!--<a href="" class="bookbtn mt1">-->
                                <!--Details-->
                            <!--</a>        -->
                        <!--</div>-->
                        <!--<div class="labelleft2">            -->
                            <!--<a href="" class="tour-search-title">                             -->
                                <!--<b>A tour to Bangkok Opera</b>-->
                            <!--</a>-->
                            <!--<p class="grey tour-description">-->
                                <!--Bangkok Opera may be just six years old, but it has already accomplished more than companies many times its age. Founded by present director Somtow Sucharitkul, it has mounted operas...-->
                            <!--</p>-->
                            <!--<ul class="hotelpreferences">-->
            <!--<li class="active" title="Has Insurance">-->
            <!--<i class="amujamu-insurence"></i>Insurance-->
        <!--</li>-->
                <!--<li title="Child Not Allow">-->
            <!--<i class="amujamu-child"></i>Child-->
        <!--</li>-->
                <!--<li class="active" title="Tour Has Foods Facility">-->
            <!--<i class="amujamu-food"></i>Food-->
        <!--</li>-->
                <!--<li title="No Pickup Services">-->
            <!--<i class="amujamu-bus"></i>Pickup-->
        <!--</li>-->
                <!--<li class="active" title="Booking Request Available">-->
            <!--<i class="amujamu-book"></i>Seat Request-->
        <!--</li>-->
    <!--</ul>                        -->
                        <!--</div>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->

<!--<h1><b>Tour Details</b></h1>-->
<!--<hr>-->
<!--<div class="row">-->
  <!--<div class="col-xs-9">-->
<!--<h3><b>Tiltle</b></h3>-->
<!--<p>{{title}}</p>-->


<!--<h3><b>Description</b></h3>-->
<!--<p><i>{{description}}</i></p>-->

<!--</div>-->

<!--<div class="col-xs-5">-->
<!--<h3><b>Country</b></h3>-->
<!--<p>{{country}}</p>-->
<!--</div>-->
<!--<div class="col-xs-6">-->
<!--<h3><b>Duration</b></h3>-->
<!--<p>{{duration}}</p>-->
<!--</div>-->

<!--</div>-->

  <!--<h3><b>Food</b></h3>-->
  <!---->
<!--<div *ngFor="let food of foods">-->
<!--<li>-->
<!--<ul><i>{{food.name}}</i></ul>-->
<!--</li>-->
<!--</div>-->
<!--<h3><b>Cancellation Policy</b></h3>-->

<!--<p>{{cancel}}</p>-->
  `,


    providers: [TourService]
})
export class CategoryComponent {


    //id: number;
    title: string;
    description: string;
     country:string;
    // duration:string;
    // price:string;
    // foods:Food[];
    // cancel:string;
    tours:Tour[];

    mytours: Tour[];
    sub:any;
    id:any;
    searchTerm:any;


    constructor(private tourService :TourService, private route: ActivatedRoute){

        this.sub = this.route.params.subscribe(params => {
            this.searchTerm = params['searchTerm'];
            this.id =params['id'];
        });

        this.tourService.getTour(this.searchTerm,this.id).subscribe(
            tours=>
            {             console.log(tours);
                this.mytours = tours;

            });
        document.title = "Hello World";

    }

    // performSearch(searchTerm: HTMLInputElement): void {
    //
    //     console.log(searchTerm.value);
    //
    //
    //     this.tourService.getAllTour().subscribe(
    //         tours => {
    //             //console.log(tours.slug);
    //             this.mytours = tours;
    //
    //
    //         });


    // };
}

interface Tour{
    id :string;
    title:string;
    name:string;


}

