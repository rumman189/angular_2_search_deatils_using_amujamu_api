"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var tour_service_1 = require("../services/tour.service");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var UserComponent = (function () {
    // country:string;
    // duration:string;
    // price:string;
    // foods:Food[];
    // cancel:string;
    // tours:Tour[];
    // mytours: Tour[];
    function UserComponent(router) {
        this.router = router;
        document.title = 'hello amujamu';
        // this.tourService.getTour().subscribe(
        //     tours=>{
        //         // this.tours=tours;
        //         this.mytours=tours;
        //         // this.description=tours.tour.description,
        //         // this.country=tours.tour.country.country,
        //         // this.duration=tours.tour.tour_duration.duration,
        //         // this.foods=tours.tour.foods,
        //         // this.cancel=tours.tour.cancellation_policy;
        //
        //
        //     });
    }
    //
    UserComponent.prototype.performSearch = function (searchTerm) {
        console.log(searchTerm.value);
        this.router.navigate(['./search/' + searchTerm.value]);
        //
        //     this.tourService.getAllTour(searchTerm.value).subscribe(
        //         tours => {
        //             console.log(tours);
        //             this.mytours = tours;
        //              // this.has_picuplocation=tours.tour.has_picuplocation;
        //
        //         });
        // //
        //
        // };
        // add(searchTerm: HTMLInputElement):void{
        //     console.log(searchTerm.value);
    };
    return UserComponent;
}());
UserComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'user',
        templateUrl: 'user.component.html',
        providers: [tour_service_1.TourService, platform_browser_1.Title]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], UserComponent);
exports.UserComponent = UserComponent;
// interface Tour{
//    id :string;
//    title:string;
//    name:string;
//    has_picuplocation:boolean;
//    is_child_allowed:boolean;
//    has_insurance:boolean;
//    request_mode:boolean;
// }
//
// interface Food{
//
//     id :any;
//     name :string;
//
// }
//# sourceMappingURL=user.component.js.map