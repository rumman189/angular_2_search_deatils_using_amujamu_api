"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var abouttour_service_1 = require("../services/abouttour.service");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var AboutComponent = (function () {
    function AboutComponent(tourService, route) {
        var _this = this;
        this.tourService = tourService;
        this.route = route;
        document.title = "Hello World";
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            _this.country = params['country'];
            _this.category = params['category'];
            _this.slug = params['slug'];
        });
        this.tourService.getTour(this.id, this.country, this.category, this.slug).subscribe(function (tours) {
            console.log(tours);
            //this.title=tours.tour.title,
            _this.description = tours.tour.description,
                _this.country = tours.tour.country.country;
            _this.duration = tours.tour.tour_duration.duration;
            _this.foods = tours.tour.foods;
            _this.feature = tours.tour.included_features;
            _this.cancel = tours.tour.cancellation_policy;
            _this.rating = tours.tour.rating;
            _this.reviews = tours.tour.reviews;
        });
        // console.log(this.tours);
        // title.setTitle('My App - Page Title');
        document.title = "Hello World";
    }
    return AboutComponent;
}());
AboutComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'about',
        templateUrl: 'about.component.html',
        //     selector: 'about',
        //     template:`
        //
        //
        // <head>
        //     <title>angular5 | Take a trip, Take a breath</title>
        //     <base href="/">
        //     <META NAME="Title" CONTENT="angular5 | Take a trip, Take a breath">
        //     <META NAME="Description" CONTENT="angular2 is an innovative services at any time from any location across the globe. Stay tension free about your tour bookings.">
        //     <link rel="shortcut icon" href="./home-bg.ico" alt="image"/>
        //
        // </head>
        //     <h3>Tour Details</h3>
        //     <p>{{title}}</p>
        //     <div class="rateYo"></div>
        //     <p>{{rating}}</p>
        //     <h3>Description</h3>
        //     <p>{{description}}</p>
        //     <h3>Cancellation Policy</h3>
        // <p>{{cancel}}</p>
        //
        //
        // <div>
        //         <h3>Country</h3>
        //         <p>{{country}}</p>
        //         <h3>Duration</h3>
        //         <p>{{duration}}</p>
        // </div>
        //
        //         <h3>Food </h3>
        //         <div *ngFor="let food of foods">
        //           <p>{{food.name}}</p>
        //
        //         </div>
        //
        //
        //
        //       <div>
        //         <h3>Feature </h3>
        //
        //        <p>{{feature}}</p>
        //
        //         </div>
        //         <hr/>
        //  <div class =row>
        //
        //
        // <h2>Review </h2>
        //
        //         <div *ngFor="let review of reviews">
        //         <div class =row>
        //          <div class="col-sm-3">
        //        <a href="#">
        //                         <img class="img-responsive" src="../././img/post-sample-image.jpg" alt="" height="100px" width="500px">
        //                     </a>
        //                     </div>
        //         <div class="col-sm-6">
        //         <p> {{review.created_at | date:'medium'}}</p>
        //           <p>{{review.user.email}}</p>
        //           <hr/>
        //           <h4>Comment :</h4>
        //           <h5>{{review.comment}}</h5>
        //            <hr/>
        //           </div>
        //
        //           <div class="col-sm-3">
        //           <h3>Rating</h3>
        //           <p>{{review.rating}}</p>
        // </div>
        //           </div>
        //           </div>
        //
        //
        //
        //         </div>
        //
        //
        //      `,
        providers: [abouttour_service_1.TourService, platform_browser_1.Title]
    }),
    __metadata("design:paramtypes", [abouttour_service_1.TourService, router_1.ActivatedRoute])
], AboutComponent);
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.component.js.map