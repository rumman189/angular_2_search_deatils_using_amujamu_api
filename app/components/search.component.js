"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var tour_service_1 = require("../services/tour.service");
var router_1 = require("@angular/router");
var SearchComponent = (function () {
    function SearchComponent(tourService, route) {
        var _this = this;
        this.tourService = tourService;
        this.route = route;
        this.searchTerm = route.snapshot.params['searchTerm'];
        //this.searchTerm = searchTerm;
        this.tourService.getAllTour(this.searchTerm).subscribe(function (tours) {
            console.log(tours);
            _this.mytours = tours;
            //              // this.has_picuplocation=tours.tour.has_picuplocation;
            //
        });
        document.title = "Hello World";
    }
    SearchComponent.prototype.performSearch = function (searchTerm) {
        console.log(searchTerm.value);
        //     this.tourService.getAllTour(searchTerm.value).subscribe(
        //         tours => {
        //             console.log(tours);
        //             this.mytours = tours;
        //              // this.has_picuplocation=tours.tour.has_picuplocation;
        //
        //         });
        //
        //
    };
    ;
    return SearchComponent;
}());
SearchComponent = __decorate([
    core_1.Component({
        selector: 'user',
        template: "\n<div class=\"row\">\n<div class=\"col-sm-3\">\n<input class=\"form-control\"name=\"search\" #searchTerm (keyup.enter)=\"performSearch(searchTerm)\">\n        \n<button type=\"button\" [routerLink]=\"['/search',searchTerm.value]\" class=\"btn btn-success\" >Search</button><hr/>\n<br/>\n<br/>\n<h5>Filter By Categories </h5>\n\n\n\n<ul>\n<li *ngFor=\"let t of mytours\" >\n  <a [routerLink]=\"['/category',searchTerm.value,t.category.id]\">{{t.category.name}} </a>\n  \n            </li>\n                    <!--<li>-->\n            <!---->\n                            <!--<a [routerLink]=\"['/category',searchTerm.value,10]\">Spot Visit </a>-->\n            <!--</li>-->\n                    <!--<li>-->\n            <!---->\n                            <!--<a [routerLink]=\"['/category',searchTerm.value,11]\">Day Tour </a>-->\n            <!--</li>-->\n                    <!--<li>-->\n            <!--<span></span>-->\n                            <!--<a>Nature </a>-->\n            <!--</li>-->\n            </ul>\n            \n\n</div>\n<!--<div *ngFor=\"let tour of tours\">-->\n<!--<li><a routerLink=\"/about\" >{{tour.title}}</a>-->\n\n<!--</li>-->\n<!--</div>-->\n<!--<a routerLink=\"/about\" >Details</a>-->\n<!--<div>-->\n\n\n\n<div class=\"col-sm-9\">\n<div class=\"row\">\n\n \n \n<div class=\"jumbotron\"  *ngFor=\"let t of mytours\"\n  [routerLink]=\"['/about', t.id,t.country.country,t.category.slug,t.slug]\">\n  <div class=\"row\">\n  <div class=\"col-sm-3\">\n  <a href=\"#\">\n                        <img class=\"img-responsive\" src=\"../././img/post-sample-image.jpg\" alt=\"\" height=\"100px\" width=\"500px\">\n                    </a>\n                    </div>\n                    <div class=\"col-sm-6\">\n <a><b><h3>{{ t.title }}</h3></b></a>\n <h5>Rating: {{t.rating}}</h5><br/>\n\n\n <h5 >\n {{t.description | limitTo: 50 }}<br/>\n\n </h5>\n <h5>{{t.id}}</h5><br/>\n <span *ngIf=\"t.has_picuplocation == true || t.has_picuplocation != null && t.has_picuplocation != false \">\n <i class=\"fa fa-bus\" aria-hidden=\"true\"></i>\n </span>\n <span *ngIf=\"t.is_child_allowed == true || t.is_child_allowed != null && t.is_child_allowed != false\">\n<i class=\"fa fa-child\" aria-hidden=\"true\"></i>\n</span>\n<span *ngIf=\"t.has_insurance == true || t.has_insurance != null && t.has_insurance != false\">\n<i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n</span>\n<span *ngIf=\"t.request_mode == true || t.request_mode != null && t.request_mode != false\">\n<i class=\"fa fa-envelope\"></i>\n</span>\n<span *ngIf=\"t.foods?.length > 0\">\n<i class=\"fa fa-cutlery\" aria-hidden=\"true\"></i>\n</span>\n </div>\n <div class=\"col-sm-2\">\n    <h6>Amujamu Price</h6>\n   <h5>{{t.default_price}}</h5>\n   <h6>Market Price</h6>\n <h5><font color=\"red\"><del>{{t.original_market_price}}</del></font></h5><br/>\n\n  <a><button type=\"button\" class=\"btn btn-primary btn-xs\">Details</button></a>\n  \n  </div>\n  </div>\n </div>\n </div>\n </div>\n </div>\n \n <!--<br/> {{t.default_price}}-->\n <!--<br/> {{t.original_market_price}}-->\n <!--<br/> {{t.total_rating}}-->\n\n   <!--<i class=\"fa fa-refresh fa-spin fa-3x fa-fw\" aria-hidden=\"true\"></i>-->\n<!--<span class=\"sr-only\">Refreshing...</span>-->\n<!--<i class=\"fa fa-bus\" aria-hidden=\"true\"></i>-->\n<!--<i class=\"fa fa-child\" aria-hidden=\"true\"></i>-->\n<!--<i class=\"fa fa-home\" aria-hidden=\"true\"></i>-->\n \n\n \n \n  \n  \n   \n\n\n\n\n\n   <!--<div *ngFor=\"let p of mytours\">-->\n<!--<p>{{p.slug}}</p>-->\n  <!---->\n\n        <!---->\n         <!---->\n          <!---->\n        <!--</div>-->\n          <!---->\n       <!---->\n  <!--</div>-->\n  <!---->\n  \n<!--<div class=\"clearfix\"></div>-->\n    <!--<div class=\"itemscontainer offset-1\">-->\n                    <!--<div class=\"offset-2\">-->\n                <!--<div class=\"col-md-4 offset-0\">-->\n                    <!--<div class=\"listitem2\">-->\n                                                    <!--<a href=\"\">-->\n                                <!--<img src=\"\" alt=\"A tour to Bangkok Opera\"/>-->\n                            <!--</a>-->\n                                            <!--</div>-->\n                <!--</div>-->\n                <!--<div class=\"col-md-8 offset-0\">-->\n                    <!--<div class=\"itemlabel3\">-->\n                        <!--<div class=\"labelright\">-->\n                            <!--<div class=\"rate-star no-padding\" data-rating-value=\"2.8\"></div>-->\n                            <!--<span class=\"size11 grey\">1 Reviews</span>-->\n                            <!--<br><br>-->\n\n                                <!--<span class=\"size11 grey\">Amujamu Price</span><br>-->\n                                <!--<span class=\"green size16\"><b>$90.21</b></span><br>-->\n                            <!---->\n                            <!--<div class=\"clearfix\"></div>-->\n                                <!--<span class=\"size11 grey\">Market Price</span><br>-->\n                                <!--<span class=\"red size16\">-->\n                                    <!--<strike>-->\n                                        <!--$86.33-->\n                                    <!--</strike>-->\n                                <!--</span>-->\n                                <!--<br/>-->\n                                                        <!--<br>-->\n                            <!--<a href=\"\" class=\"bookbtn mt1\">-->\n                                <!--Details-->\n                            <!--</a>        -->\n                        <!--</div>-->\n                        <!--<div class=\"labelleft2\">            -->\n                            <!--<a href=\"\" class=\"tour-search-title\">                             -->\n                                <!--<b>A tour to Bangkok Opera</b>-->\n                            <!--</a>-->\n                            <!--<p class=\"grey tour-description\">-->\n                                <!--Bangkok Opera may be just six years old, but it has already accomplished more than companies many times its age. Founded by present director Somtow Sucharitkul, it has mounted operas...-->\n                            <!--</p>-->\n                            <!--<ul class=\"hotelpreferences\">-->\n            <!--<li class=\"active\" title=\"Has Insurance\">-->\n            <!--<i class=\"amujamu-insurence\"></i>Insurance-->\n        <!--</li>-->\n                <!--<li title=\"Child Not Allow\">-->\n            <!--<i class=\"amujamu-child\"></i>Child-->\n        <!--</li>-->\n                <!--<li class=\"active\" title=\"Tour Has Foods Facility\">-->\n            <!--<i class=\"amujamu-food\"></i>Food-->\n        <!--</li>-->\n                <!--<li title=\"No Pickup Services\">-->\n            <!--<i class=\"amujamu-bus\"></i>Pickup-->\n        <!--</li>-->\n                <!--<li class=\"active\" title=\"Booking Request Available\">-->\n            <!--<i class=\"amujamu-book\"></i>Seat Request-->\n        <!--</li>-->\n    <!--</ul>                        -->\n                        <!--</div>-->\n                    <!--</div>-->\n                <!--</div>-->\n            <!--</div>-->\n\n<!--<h1><b>Tour Details</b></h1>-->\n<!--<hr>-->\n<!--<div class=\"row\">-->\n  <!--<div class=\"col-xs-9\">-->\n<!--<h3><b>Tiltle</b></h3>-->\n<!--<p>{{title}}</p>-->\n\n\n<!--<h3><b>Description</b></h3>-->\n<!--<p><i>{{description}}</i></p>-->\n\n<!--</div>-->\n\n<!--<div class=\"col-xs-5\">-->\n<!--<h3><b>Country</b></h3>-->\n<!--<p>{{country}}</p>-->\n<!--</div>-->\n<!--<div class=\"col-xs-6\">-->\n<!--<h3><b>Duration</b></h3>-->\n<!--<p>{{duration}}</p>-->\n<!--</div>-->\n\n<!--</div>-->\n\n  <!--<h3><b>Food</b></h3>-->\n  <!---->\n<!--<div *ngFor=\"let food of foods\">-->\n<!--<li>-->\n<!--<ul><i>{{food.name}}</i></ul>-->\n<!--</li>-->\n<!--</div>-->\n<!--<h3><b>Cancellation Policy</b></h3>-->\n\n<!--<p>{{cancel}}</p>-->\n  ",
        providers: [tour_service_1.TourService]
    }),
    __metadata("design:paramtypes", [tour_service_1.TourService, router_1.ActivatedRoute])
], SearchComponent);
exports.SearchComponent = SearchComponent;
//# sourceMappingURL=search.component.js.map