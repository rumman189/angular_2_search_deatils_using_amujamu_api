import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class TourService {

    constructor( private http:Http){
        console.log('PostService initialized....');
    }

    getTour(id:any,country:any,category:any,slug:any){
        return this.http.get('http://202.191.125.158/en/tour/' + id +'/' + country +'/' + category + '/' +slug)

            .map(res => res.json());
    }

}
