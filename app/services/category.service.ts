import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class TourService {

    constructor( private http:Http){
        console.log('PostService initialized....');
    }

    getTour(searchTerm:any,id:any){
        return this.http.get('http://202.191.125.158/en/search?q='+ searchTerm +'&category='+ id +'&page=1')

            .map(res => res.json());
    }

}