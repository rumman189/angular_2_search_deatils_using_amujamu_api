import { NgModule }      from '@angular/core';
import { BrowserModule,Title} from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { UserComponent }  from './components/user.component';
import {HttpModule} from '@angular/http';
import {routing} from "./app.routing";
import {AboutComponent} from "./components/about.component";
import {FormsModule} from "@angular/forms";
import {CategoryComponent} from "./components/category.component";
import {TruncatePipe} from "./app.pipe";
import {SearchComponent} from "./components/search.component";

@NgModule({
  imports:      [ BrowserModule, HttpModule ,routing],
  declarations: [ AppComponent , UserComponent ,AboutComponent,CategoryComponent,TruncatePipe,SearchComponent],
  providers: [
    Title
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
